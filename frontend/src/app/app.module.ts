import {BrowserModule} from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {WalletComponent} from './wallet/wallet.component';
import {TransactionsListComponent} from './wallet/transactions/transactions-list/transactions-list.component';
import {TransactionsComponent} from './wallet/transactions/transactions.component';
import {CategoriesComponent} from './wallet/categories/categories.component';
import {CategoriesListComponent} from './wallet/categories/categories-list/categories-list.component';
import {CategoryComponent} from './wallet/categories/categories-list/category/category.component';

import {CommonModule} from "@angular/common";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatButtonModule} from "@angular/material/button";
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatTableModule} from "@angular/material/table";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatMenuModule} from "@angular/material/menu";
import {MatNativeDateModule} from "@angular/material/core";
import {MatTooltipModule} from "@angular/material/tooltip";
import {ReactiveFormsModule} from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { StringToIconTypePipe } from './string-to-icon-type.pipe';
import {MatSnackBar, MatSnackBarModule} from "@angular/material/snack-bar";

registerLocaleData(localePl, 'pl-PL');

@NgModule({
  declarations: [
    AppComponent,
    WalletComponent,
    TransactionsListComponent,
    TransactionsComponent,
    CategoriesComponent,
    CategoriesListComponent,
    CategoryComponent,
    StringToIconTypePipe
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    FontAwesomeModule,
    MatTableModule,
    MatBottomSheetModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatCheckboxModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [
    { provide: MatBottomSheetRef, useValue: {} },
    { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} },
    { provide: LOCALE_ID, useValue: 'pl-PL'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
