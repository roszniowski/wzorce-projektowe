import {Category} from "../categories/category.model";
import {Transaction as transactionModel} from "../transactions/transaction.model";
import {HttpClient} from "@angular/common/http";
import {API} from "../../../environments/environment";
import {Observable} from "rxjs";

interface Transaction {
  operation(http: HttpClient, transaction: transactionModel, category: Category): Observable<object>;
}

export abstract class Creator {

  public abstract factoryMethod(): Transaction;

  public addTransaction(http: HttpClient, transactionModel: transactionModel, category: Category) {
    const transaction = this.factoryMethod();
    return transaction.operation(http, transactionModel, category);
  }
}

export class ConcreteExpenseCreator extends Creator {
  public factoryMethod() {
    return new ExpenseTransaction();
  }
}

export class ConcreteIncomeCreator extends Creator {
  public factoryMethod(): Transaction {
    return new IncomeTransaction();
  }
}

class ExpenseTransaction implements Transaction {

  public operation(http: HttpClient, transaction: transactionModel, category: Category) {
    return http.post(API.url + 'transactions/add', {
      transaction: transaction,
      category: category.id,
      type: 1
    });
  }
}

class IncomeTransaction implements Transaction {
  public operation(http: HttpClient, transaction: transactionModel, category: Category) {
    return http.post(API.url + 'transactions/add', {
      transaction: transaction,
      category: category.id,
      type: 2
    });
  }
}
