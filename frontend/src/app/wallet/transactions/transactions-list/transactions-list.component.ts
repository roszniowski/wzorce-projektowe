import { Component, OnInit } from '@angular/core';
import {MatBottomSheetRef} from "@angular/material/bottom-sheet";
import { Transaction } from '../transaction.model';
import {WalletService} from "../../wallet.service";

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss']
})
export class TransactionsListComponent implements OnInit {
  displayedColumns: string[] = ['category', 'createdAt', 'description', 'value'];
  dataSource: Transaction[] = [];
  constructor(private _bottomSheetRef: MatBottomSheetRef<TransactionsListComponent>, private walletService: WalletService) { }

  ngOnInit(): void {
    this.walletService.getTransactionList().subscribe(
      (transactions: Transaction[]) => {
        this.dataSource = transactions['list'];
        console.log(this.dataSource);
      }
    )
  }

}
