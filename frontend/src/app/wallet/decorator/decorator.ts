export interface Logger {
  operation(): string;
}

export class ConcreteLogger implements Logger {
  public operation(): string {
    return 'ConcreteLogger';
  }
}

class Decorator implements Logger {
  protected logger: Logger;

  constructor(logger: Logger) {
    this.logger = logger;
  }

  public operation(): string {
    return this.logger.operation();
  }
}

export class ConcreteDecoratorExpense extends Decorator {
  public operation(): string {
    return 'Wydatek został zapisany.';
  }
}

export class ConcreteDecoratorIncome extends Decorator {
  public operation(): string {
    return 'Dochód został zapisany.';
  }
}
