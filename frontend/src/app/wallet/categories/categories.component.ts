import {Component, OnInit} from '@angular/core';
import {faAlignJustify, faEllipsisV, faMinus, faPlus} from "@fortawesome/free-solid-svg-icons";
import {TransactionsListComponent} from "../transactions/transactions-list/transactions-list.component";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {WalletService} from '../wallet.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  lines = faAlignJustify;
  plus = faPlus;
  minus = faMinus;
  menu = faEllipsisV;
  balance = 0;

  constructor(private _bottomSheet: MatBottomSheet, private walletService: WalletService) {
  }

  ngOnInit(): void {
    this.walletService.getIndexData().subscribe(
      (data) => {
        this.balance = data['balance']['balance'];
      },
      (error) => {
        console.log(error);
      }
    );
  }

  showHistory(): void {
    this._bottomSheet.open(TransactionsListComponent);
  }

}
