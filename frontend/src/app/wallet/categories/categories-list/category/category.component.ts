import {Component, Input, OnInit} from '@angular/core';
import {Category} from "../../category.model";
import {MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {Router} from "@angular/router";
import {WalletService} from "../../../wallet.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  @Input() category: Category;

  constructor(
    private bottomSheetRef: MatBottomSheetRef<CategoryComponent>,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  onCategorySelect(category: Category) {
    if(this.bottomSheetRef.instance) {
      this.bottomSheetRef.dismiss(category);
    } else {
      this.router.navigate(['/expense', category.id]);
    }
  }
}
