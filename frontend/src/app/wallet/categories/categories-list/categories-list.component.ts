import {Component, Inject, OnInit} from '@angular/core';
import {Category} from "../category.model";
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import {WalletService} from "../../wallet.service";

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {
  categories: Category[];
  type: Category['type'];

  constructor(private walletService: WalletService, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.walletService.getIndexData(this.data).subscribe(
      (data) => {
        this.categories = data['categories'];
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
