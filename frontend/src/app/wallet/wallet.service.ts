import {Injectable} from '@angular/core';
import {Category} from './categories/category.model';
import {API} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Transaction} from "./transactions/transaction.model";
import {ConcreteExpenseCreator, ConcreteIncomeCreator, Creator} from "./factory-method/factory-method";

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  type: Category['type'];
  category: Category;
  balance: number;
  categories: Category[];

  constructor(private http: HttpClient) {
  }

  addTransaction(transaction: Transaction, category: Category) {
    let creator: Creator;
    switch (category.type) {
      case 1:
        creator = new ConcreteExpenseCreator();
        break;
      case 2:
        creator = new ConcreteIncomeCreator();
        break;
    }
    return creator.addTransaction(this.http, transaction, category);
  }

  getTransactionList() {
    return this.http.get(API.url + 'transactions/list');
  }

  getIndexData(data?) {
    return this.http.get(API.url + 'index', {
      params: {
        type: data && data['type'] ? data['type'] : '1'
      }
    });
  }

  getCategory(category: Category) {
    return this.http.get(API.url + 'transactions', {
      params: {
        category: category.id.toString()
      }
    });
  }
}
