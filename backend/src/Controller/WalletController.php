<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WalletController extends AbstractController
{
    private $categoryRepository;
    private $transactionService;

    /**
     * WalletController constructor.
     * @param CategoryRepository $categoryRepository
     * @param TransactionService $transactionService
     */
    public function __construct(CategoryRepository $categoryRepository, TransactionService $transactionService)
    {
        $this->categoryRepository = $categoryRepository;
        $this->transactionService = $transactionService;
    }

    /**
     * @Route("index", name="index", methods={"GET", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $type = $request->query->get('type');
        return $this->json([
            'balance' => $this->transactionService->getBalance(),
            'categories' => isset($type) ? $this->categoryRepository->findBy(['type' => $type]) : $this->categoryRepository->findBy(['type' => 1])
        ]);
    }

    /**
     * @Route("/transactions", name="transactions", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexTransaction(Request $request){
        $id = $request->query->get('category');
        if(!isset($id)){
            return $this->json([
                'category' => null
            ]);
        }
        return $this->json([
            'category' => $this->categoryRepository->findOneBy(['id' => $id])
        ]);
    }

    /**
     * @Route("/transactions/list", name="list", methods={"GET"})
     * @return JsonResponse
     */
    public function getTransactionList(){
        return $this->json([
            'list' => $this->transactionService->getTransactionsList()
        ]);
    }

    /**
     * @Route("/transactions/add", name="add", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addTransaction(Request $request){
        $params = $request->getContent();
        if(!isset($params)){
            return $this->json([
                'message' => 'Transakcja nie została dodana. Dane transakcji niepoprawne.'
            ]);
        }
        $transactionData = json_decode($params, true);
        $entry = $this->transactionService->addTransaction($transactionData);
        return $this->json([
            'message' => 'Transakcja nr '.$entry.' została dodana.',
        ]);
    }
}
